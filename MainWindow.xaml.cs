using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Timerp1
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer1 = new DispatcherTimer();
        DispatcherTimer timer2 = new DispatcherTimer();
        int Crescendo = 1;
        public MainWindow()
        {
            timer1.Interval = TimeSpan.FromMilliseconds(1);
            timer1.Tick += Timer1_Tick;

            timer2.Interval = TimeSpan.FromMilliseconds(1);
            timer2.Tick += Timer2_Tick;

            InitializeComponent();
            timer1.Start();
            timer2.Start();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (L1.FontSize > 500)
                Crescendo = -1;
            if (L1.FontSize < 500)
                Crescendo += 1;
            L1.FontSize = L1.FontSize + (L1.FontSize * 0.5 * Crescendo);

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            L1.Content = (Convert.ToInt32(L1.Content) + 1).ToString();
        }
    }
}